# -*- mode: ruby -*-
# vi: set ft=ruby :
## Instructions: 
## Open some tickets to IT to get VirtualBox and Vagrant installed 
## Ask IT to run this command in a PowerShell console with Admin privileges: Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
## Place this file in your user's Windows home folder E.g. C:\Users\YOUR.NAME\Vagrantfile
## Manually create a RSA Key pair using PuTTYgen then convert it from .ppk(win) to .pem(linux) format
## No passwords should be used, only SSH keys with no passphrase.
## Place the SSH key pair in your user's home dir in Windows and rename them to id_rsa and id_rsa.pub 
## Probably we won't have root privileges on the App VMs, so handle everything using the robotic id and app's service accounts without root/vagrant permissions 
## Probably we won't have permissions to install software with yum, instead use Nexus to upload/download App's libs & dependencies
## Finally create your VMs by running from a PowesrShell: vagrant up
## Note: IP addresses should be on the same network as your VirtualBox Ethernet adapter
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'
Vagrant.configure("2") do |config|
  ##### DEFINE DEV VMs #####################
  begin
    config.vm.define "loadbalancer" do |config|
      config.vm.hostname = "app.softtek.com"
      config.vm.network "private_network", ip: "192.168.56.10"
      config.vm.provider :virtualbox do |domain|
        domain.cpus = "1"
        domain.memory = "1024"
      end
	  config.vm.provision "shell", inline: "ssh-keygen -t rsa -f /shared/id_rsa -q -P '' -C robot", run: "once"
    end
	config.vm.define "appnode01" do |config|
        config.vm.hostname = "appnode01.softtek.com"
        config.vm.network "private_network", ip: "192.168.56.11"
        config.vm.provider :virtualbox do |domain|
          domain.cpus = "1"
          domain.memory = "1024"
        end
		config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/robotic_id.yml", run: "once"
		config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/app_users.yml", run: "once"
	end
		config.vm.define "appnode02" do |config|
        config.vm.hostname = "appnode02.softtek.com"
        config.vm.network "private_network", ip: "192.168.56.12"
        config.vm.provider :virtualbox do |domain|
          domain.cpus = "1"
          domain.memory = "1024"
        end
		config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/robotic_id.yml", run: "once"
		config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/app_users.yml", run: "once"		
	end
    config.vm.define "nexus" do |config|
      config.vm.hostname = "nexus.softtek.com"
      config.vm.network "private_network", ip: "192.168.56.13"
      config.vm.provider :virtualbox do |domain|
        domain.cpus = "1"
        domain.memory = "1024"
      end
	  config.vm.provision "shell", inline: "ansible-galaxy install geerlingguy.java", run: "once"
	  config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/OpenJDK8.yml", run: "once"
	  config.vm.provision "shell", inline: "ansible-galaxy install geerlingguy.apache", run: "once"
	  config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/apache.yml", run: "once"
	  config.vm.provision "shell", inline: "ansible-galaxy install ansible-thoteam.nexus3-oss", run: "once"
	  config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/nexus3-oss.yml", run: "once"
    end
    config.vm.define "sonar" do |config|
      config.vm.hostname = "sonar.softtek.com"
      config.vm.network "private_network", ip: "192.168.56.14"
      config.vm.provider :virtualbox do |domain|
        domain.memory = "1024"
        domain.cpus = "1"
      end
	  config.vm.provision "shell", inline: "ansible-galaxy install geerlingguy.sonar", run: "once"
	  config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/sonar.yml", run: "once"
    end	
    config.vm.define "jenkins" do |config|
      config.vm.hostname = "jenkins.softtek.com"
      config.vm.network "private_network", ip: "192.168.56.15"
      config.vm.provider :virtualbox do |domain|
        domain.cpus = "1"
        domain.memory = "1024"
      end
	  config.vm.provision "shell", inline: "ansible-galaxy install geerlingguy.jenkins", run: "always"
	  config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/jenkins.yml", run: "once"
    end
  end
  ##### GENERIC VMs CONFIGURATIONS #####
  ## config.vm.box = "centos/7" ## Mainstream CentOS Official Image, No VBox guest additions
  config.vm.box = "geerlingguy/centos7"  ## Has VBox guest additions
  config.vm.box_check_update = false
  config.vm.provision "shell", inline: "echo 'PermitRootLogin no' >> /etc/ssh/sshd_config", run: "once"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder "shared", "/shared", disabled: false
  ## Install Ansible
  config.vm.provision "shell", inline: "yum install epel-release unzip -y"
  config.vm.provision "shell", inline: "yum install ansible -y"
  config.vm.provision "shell", inline: "ansible-playbook -i /shared/inventory.yml /shared/etc_hosts.yml", run: "once"
end