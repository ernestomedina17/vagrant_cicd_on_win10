# vagrant_cicd_on_win10

This is a Vagrant project on Windows 10 with 6 VirtbualBox VMs of centos/7 providing:
- 1 loadbalancer with HAProxy
- 2 app nodes
- 1 nexus
- 1 sonar
- 1 jenkins

Instructions:
- Install VirtualBox and Vagrant on Windows
- Run this command in a PowerShell console with Admin privileges: Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
- Create the VMs by running these commands from PowesrShell with a regular account: 
	- set VAGRANT_LOG=debug 
	- vagrant up

The idea is to install the infrastructure and toolset using root privileges, but for the custom app use CI/CD with regular user accounts, because usually in a 
company the Ops team will never give root privileges to developers, they would say that its like giving a gun to a kid, and they are right.
So if you can't use yum to install your app's dependencies what will you do? Use Nexus to upload/download and install your stuff.

